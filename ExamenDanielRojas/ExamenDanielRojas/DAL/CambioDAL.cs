﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using veintiuno_utn.DAL;

namespace ExamenDanielRojas.DAL
{
    class CambioDAL
    {
        /// <summary>
        /// Inserta los tipos de cambios a la base de datos.
        /// </summary>
        /// <param name="reg"></param>
        public void Insertar(Series reg)
        {
            try
            {
                string sql = " INSERT INTO tipo_cambio (cambio, fecha)" +
               "VALUES(@cam, @fecha) returning Id";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@cam", reg.Serie1);
                    cmd.Parameters.AddWithValue("@fecha", reg.dia);
                    reg.Id = (int)cmd.ExecuteScalar();
                }
            }
            catch(Exception e)
            {
                throw e;
            }          
        }
        /// <summary>
        /// Carga los tipos de cambios guardados en la base de datos.
        /// </summary>
        /// <returns></returns>
        public List<Series> CargarCambios()
        {
            try
            {
                List<Series> registros = new List<Series>();
                string sql = "SELECT cambio, fecha FROM tipo_cambio";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    NpgsqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        registros.Add(CargarCambios(reader));
                    }
                }
                return registros;
            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }
        private Series CargarCambios(NpgsqlDataReader reader)
        {
            try
            {
                Series temp = new Series
                {
                    Serie1 = reader.GetDouble(0),
                    dia = reader.GetString(1)
                };
                return temp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

      
        /// <summary>
        /// Elimina los datos de las tablas.
        /// </summary>
        public void EliminarDatos()
        {
            try
            {
                string sql = "DELETE FROM tipo_cambio";
                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
